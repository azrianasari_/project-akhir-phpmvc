$(function() {

    $('.modalTambahPemilik').on('click', function() {
        $('#formModalLabel').html('Tambah Data');
        $('.modal-footer button[type=submit]').html('Tambah Data');
        $('input[type="text"]').val('');
        $('#pricing').prop('selectedIndex',0);
    }); 

    $('.modalEditPemilik').on('click', function() {
        
        $('#formModalLabel').html('Edit Data Pemilik');
        $('.modal-footer button[type=submit]').html('Ubah Data');
        $('.form-modal').attr('action','http://localhost/projectAkhir/phpMVC/public/pemilik/ubah')

        const id = $(this).data('id');
        $.ajax({
            url: 'http://localhost/projectAkhir/phpMVC/public/pemilik/getUbah',
            data : { id : id }, 
            method : 'post',
            dataType : 'json',
            success : function(data) {
                $('#nama').val(data.nama);
                $('#alamat').val(data.alamat);
                $('#kelurahan').val(data.kelurahan);
                $('#kecamatan').val(data.kecamatan);
                $('#kab_kota').val(data.kab_kota);
                $('#kode').val(data.kode);
                $('#kode_pos').val(data.kode_pos);
                $('#email').val(data.email);
                $('#telp').val(data.telp);
                $('#id').val(data.id);      
            }
        })

    });
    
    
    // mobil
    $('.modalTambahMobil').on('click', function() {
        $('#formModalLabel').html('Tambah Data');
        $('.modal-footer button[type=submit]').html('Tambah Data');
        $('input[type="text"]').val('');
        $('#id_pemilik').prop('selectedIndex',0);
    });
    
    $('.modalEditMobil').on('click', function() {
        
        $('#formModalLabel').html('Edit Data Mobil');
        $('.modal-footer button[type=submit]').html('Ubah Data');
        $('.form-modal').attr('action','http://localhost/projectAkhir/phpMVC/public/mobil/ubah')
       
        const id = $(this).data('id');
        
        $.ajax({
            url: 'http://localhost/projectAkhir/phpMVC/public/mobil/getUbah',
            data : { id : id }, 
            method : 'post',
            dataType : 'json',
            success : function(data) {
                console.log(data)
                $('#kode').val(data.kode);
                $('#tahun').val(data.tahun);
                $('#warna').val(data.warna);
                $('#no_plat').val(data.no_plat);
                $('#no_mesin').val(data.no_mesin);
                $('#no_rangka').val(data.no_rangka);
                $('#status_mobil').val(data.status_mobil);
                $('#merk').val(data.merk);
                $('#tipe').val(data.tipe);
                $('#pemilik_id').val(data.pemilik_id).trigger('change');
                $('#id').val(data.id);  
            }
        })

        console.log($(this).data);

    });


      // users
      $('.modalTambahUser').on('click', function() {
        $('#formModalLabel').html('Tambah Data');
        $('.modal-footer button[type=submit]').html('Tambah Data');
        $('input[type="text"]').val('');
        $('#id_pegawai').prop('selectedIndex',0);
    });

    $('.modalEditUser').on('click', function() {
        
        $('#formModalLabel').html('Edit Data User');
        $('.modal-footer button[type=submit]').html('Ubah Data');
        $('.form-modal').attr('action','http://localhost/projectAkhir/phpMVC/public/users/ubah')

        const id = $(this).data('id');
        $.ajax({
            url: 'http://localhost/projectAkhir/phpMVC/public/users/getUbah',
            data : { id : id }, 
            method : 'post',
            dataType : 'json',
            success : function(data) {
                $('#username').val(data.username);
                $('#jabatan').val(data.jabatan);
                $('#id').val(data.id);  
              
            }
        })

    });

});