
    <div class="container mt-5">
        <div class="row">
            <div class="col-lg-12">
              <?php Flasher::flash() ?>
            </div>
            <div class="col-lg-12">
                <h4>Daftar Pemilik</h4>
            </div>

            <div class="col-lg-6 mb-2">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary modalTambahPemilik" data-toggle="modal" data-target="#formModal">
                  Tambah Data
                </button>
            </div>

        </div>
        <div class="row">   
            <table border=1 cellpadding="20">
                <thead style="text-align:center">
                    <th>No</th>
                    <th>Nama</th>
                    <th>Alamat</th>
                    <th>Telepon</th>
                    <th>Aksi</th>
                </thead>
                <tbody>
                <?php $index = 1 ?>
                <?php foreach ($data['pemilik'] as $dt) : ?>
                    <tr>
                        <td><?= $index++ ?>  </td>   
                        <td><?= $dt['nama'] ?> </td>   
                        <td><?= $dt['alamat'] ?> </td>   
                        <td><?= $dt['telp'] ?> </td> 
                        <td>
                          <a href="#" class="modalEditPemilik" data-toggle="modal" data-target="#formModal" data-id="<?= $dt['id'] ?>" >Edit</a> |
                          <a href="<?= BASEURL?>/pemilik/delete/<?= $dt['id'] ?>" onclick="return confirm('Yakin?')">Hapus</a> |
                          <a href="<?= BASEURL ?>/pemilik/detail/<?= $dt['id']?>">Detail</a>
                        </td>  
                    </tr> 
                <?php endforeach; ?>  
                </tbody>
            </table>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="formModal" tabindex="-1" aria-labelledby="formModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="formModalLabel">Tambah Data</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form class="form-modal" action="<?= BASEURL ?>/pemilik/tambah" method="post" enctype="multipart/form-data">
             <input type="hidden" id="id" name="id">
             <div class="modal-body">
               <div class="form-group">
                 <label for="kode">Kode</label>
                 <input type="text" class="form-control" id="kode" name="kode" placeholder="kode">
               </div>
               <div class="form-group">
                 <label for="nama">Nama</label>
                 <input type="text" class="form-control" id="nama" name="nama" placeholder="nama">
               </div>
               <div class="form-group">
                 <label for="alamat">Alamat</label>
                 <input type="text" class="form-control" id="alamat" name="alamat" placeholder="alamat">
               </div>
               <div class="form-group">
                 <label for="kecamatan">Kecamatan</label>
                 <input type="text" class="form-control" id="kecamatan" name="kecamatan" placeholder="kecamatan">
               </div>
               <div class="form-group">
                 <label for="kelurahan">Kelurahan</label>
                 <input type="text" class="form-control" id="kelurahan" name="kelurahan" placeholder="kelurahan">
               </div>
               <div class="form-group">
                 <label for="kab_kota">Kab/Kota</label>
                 <input type="text" class="form-control" id="kab_kota" name="kab_kota" placeholder="kab_kota">
               </div>
               <div class="form-group">
                 <label for="kode_pos">Kode Pos</label>
                 <input type="text" class="form-control" id="kode_pos" name="kode_pos" placeholder="kode_pos">
               </div>
               <div class="form-group">
                 <label for="telp">Telepon</label>
                 <input type="text" class="form-control" id="telp" name="telp" placeholder="telp">
               </div>
              
            </div>
             <div class="modal-footer">
               <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
               <button type="submit" class="btn btn-primary">Tambah Data</button>
             </div>
         </form>
      </div>
    </div>