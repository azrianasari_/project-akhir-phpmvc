<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= BASEURL ?>/assetmvc/css/bootstrap.min.css" >

    <title>Dashboard</title>
  </head>
  <body>

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <div class="container">
       <a class="navbar-brand" href="#" style="font-size:24px!important">eCar</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav " aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
          <?php if($_SESSION['jabatan'] == 0 ) { ?> 
          <li class="nav-item active">
            <a class="nav-link" href="<?= BASEURL ?>/dashboard">Dashboard</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= BASEURL ?>/pemilik">Pemilik</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= BASEURL ?>/mobil">Mobil</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= BASEURL ?>/users">Users</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= BASEURL ?>/logout">Logout</a>
          </li>
          <?php } ?>

          <?php if($_SESSION['jabatan'] == 1 ) { ?> 
          <li class="nav-item active">
            <a class="nav-link" href="<?= BASEURL ?>/dashboard">Dashboard</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= BASEURL ?>/mobil">Mobil</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= BASEURL ?>/logout">Logout</a>
          </li>
          <?php } ?>
        </ul>
      </div>
      </div>
    </nav>
