    <div class="container mt-5">
        <div class="row">
            <div class="col-lg-12">
              <?php Flasher::flash() ?>
            </div>
            <div class="col-lg-12">
                <h4>Daftar Users</h4>
            </div>

            <div class="col-lg-6 mb-2">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary modalTambahUser" data-toggle="modal" data-target="#formModal">
                  Tambah Data
                </button>
            </div>

        </div>
        <div class="row">   
            <table border=1 cellpadding="20">
                <thead style="text-align:center">
                    <th>No</th>
                    <th>Username</th>
                    <th>jabatan</th>
                    <th>Aksi</th>
                </thead>
                <tbody>
                <?php $index = 1 ?>
                <?php foreach ($data['users'] as $dt) : ?>
                    <tr>
                        <td><?= $index++ ?>  </td>   
                        <td><?= $dt['username'] ?> </td>   
                        <td><?php if($dt['jabatan'] == 0 ) {echo 'Admin';} else {echo 'Kasir';}?> </td>   
                        <td>
                          <a href="#" class="modalEditUser" data-toggle="modal" data-target="#formModal" data-id="<?= $dt['id'] ?>" >Edit</a> |
                          <a href="<?= BASEURL?>/users/delete/<?= $dt['id'] ?>" onclick="return confirm('Yakin?')">Hapus</a> 
                        </td>  
                    </tr> 
                <?php endforeach; ?>  
                </tbody>
            </table>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="formModal" tabindex="-1" aria-labelledby="formModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="formModalLabel">Tambah Data</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form class="form-modal" action="<?= BASEURL ?>/users/tambah" method="post" enctype="multipart/form-data">
             <input type="hidden" id="id" name="id">
             <div class="modal-body">
               <div class="form-group">
                 <label for="username">Username</label>
                 <input type="text" class="form-control" id="username" name="username" placeholder="username">
               </div>
               <div class="form-group">
                 <label for="password">Password</label>
                 <input type="text" class="form-control" id="password" name="password" placeholder="password">
               </div>
               <div class="form-group">
                 <label for="jabatan">Status Mobil</label>
                 <select name="jabatan" id="jabatan" class="form-control">
                     <option value="0">Admin</option>
                     <option value="1">Kasir</option>
                 </select>
               </div>
             </div>
             <div class="modal-footer">
               <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
               <button type="submit" class="btn btn-primary">Tambah Data</button>
             </div>
         </form>
      </div>
    </div>