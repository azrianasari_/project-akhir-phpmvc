<?php

// session_start();
// if( !isset($_SESSION['login'])){
//     header("Location:../login/index.php");
//     exit;
// }
?>
    <div class="container mt-5">
        <div class="row">
            <div class="col-lg-12">
            Daftar Testimonials
            </div>
        </div>
        <div class="row">   
            <table border=1 cellpadding="20">
                <thead style="text-align:center">
                    <th>No</th>
                    <th>Category</th>
                    <th>Nama</th>
                    <th>Testimoni</th>
                </thead>
                <tbody>
                <?php $index = 1 ?>
                <?php foreach ($data['testimonials'] as $dt) : ?>
                    <tr>
                        <td><?= $index++ ?>  </td>   
                        <td><?= $dt['category'] ?> </td>   
                        <td><?= $dt['nama'] ?> </td>   
                        <td><?= $dt['testimoni'] ?> </td>   
                    </tr> 
                <?php endforeach; ?>  
                </tbody>
            </table>
        </div>
    </div>

