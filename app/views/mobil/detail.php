<div class="container mt-5">

    <ul>
        <li><?= $data['mobil']['kode'] ?></li>   
        <li><?= $data['mobil']['tahun'] ?></li>   
        <li><?= $data['mobil']['warna'] ?></li> 
        <li><?= $data['mobil']['no_plat'] ?></li>   
        <li><?= $data['mobil']['no_mesin'] ?></li>   
        <li><?= $data['mobil']['no_rangka'] ?></li> 
        <li><?php if($data['mobil']['status_mobil'] == 1) {echo 'Tersedia';} else {echo 'Tidak tersedia';} ?></li>   
        <li><?= $data['mobil']['merk'] ?></li>   
        <li><?= $data['mobil']['pemilik_id'] ?></li> 
        <li><img src="<?=  BASEURL?>/img/<?= $data['mobil']['foto'] ?>" alt="" width=20%></li> 
        <li><a href="<?=  BASEURL?>/mobil">Kembali</a></li>   
    </ul>

</div>