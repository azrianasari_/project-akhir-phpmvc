
    <div class="container mt-5">
        <div class="row">
            <div class="col-lg-12">
              <?php Flasher::flash() ?>
            </div>
            <div class="col-lg-12">
                <h4>Daftar Mobil</h4>
            </div>

            <div class="col-lg-6 mb-2">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary modalTambahMobil" data-toggle="modal" data-target="#formModal">
                  Tambah Data
                </button>
            </div>

        </div>
        <div class="row">   
            <table border=1 cellpadding="20">
                <thead style="text-align:center">
                    <th>No</th>
                    <th>Kode</th>
                    <th>Status</th>
                    <th>Pemilik</th>
                    <th>Aksi</th>
                </thead>
                <tbody>
                <?php $index = 1 ?>
                <?php foreach ($data['mobil'] as $dt) : ?>
                    <tr>
                        <td><?= $index++ ?>  </td>   
                        <td><?= $dt['kode'] ?> </td>   
                        <td><?php if($dt['status_mobil'] == 1) {echo 'Tersedia';} else {echo 'Tidak tersedia';}?> </td>   
                        <td><?= $dt['nama'] ?> </td> 
                        <td>
                          <a href="#" class="modalEditMobil" data-toggle="modal" data-target="#formModal" data-id="<?= $dt['id'] ?>" >Edit</a> |
                          <a href="<?= BASEURL?>/mobil/delete/<?= $dt['id'] ?>" onclick="return confirm('Yakin?')">Hapus</a> |
                          <a href="<?= BASEURL ?>/mobil/detail/<?= $dt['id']?>">Detail</a>
                        </td>  
                    </tr> 
                <?php endforeach; ?>  
                </tbody>
            </table>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="formModal" tabindex="-1" aria-labelledby="formModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="formModalLabel">Tambah Data</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form class="form-modal" action="<?= BASEURL ?>/mobil/tambah" method="post" enctype="multipart/form-data">
             <input type="hidden" id="id" name="id">
             <div class="modal-body">
               <div class="form-group">
                 <label for="kode">Kode</label>
                 <input type="text" class="form-control" id="kode" name="kode" placeholder="kode">
               </div>
               <div class="form-group">
                 <label for="pemilik_id">Pemilik</label>
                 <select name="pemilik_id" id="pemilik_id" class="form-control">
                     <?php foreach ($data['pemilik'] as $dp) : ?>
                     <option value="<?= $dp['id'] ?>"><?= $dp['kode'] ?> - <?= $dp['nama'] ?> </option>
                     <?php endforeach; ?>  
                 </select>
               </div>
               <div class="form-group">
                 <label for="tahun">Tahun</label>
                 <input type="text" class="form-control" id="tahun" name="tahun" placeholder="tahun">
               </div>
               <div class="form-group">
                 <label for="warna">Warna</label>
                 <input type="text" class="form-control" id="warna" name="warna" placeholder="warna">
               </div>
               <div class="form-group">
                 <label for="no_plat">No Plat</label>
                 <input type="text" class="form-control" id="no_plat" name="no_plat" placeholder="no_plat">
               </div>
               <div class="form-group">
                 <label for="no_mesin">No Mesin</label>
                 <input type="text" class="form-control" id="no_mesin" name="no_mesin" placeholder="no_mesin">
               </div>
               <div class="form-group">
                 <label for="no_rangka">No Rangka</label>
                 <input type="text" class="form-control" id="no_rangka" name="no_rangka" placeholder="no_rangka">
               </div>
               <div class="form-group">
                 <label for="status_mobil">Status Mobil</label>
                 <select name="status_mobil" id="status_mobil" class="form-control">
                     <option value="0">Tersedia</option>
                     <option value="1">Tidak Tersedia</option>
                 </select>
               </div>
               <div class="form-group">
                 <label for="merk">Merk</label>
                 <input type="text" class="form-control" id="merk" name="merk" placeholder="merk">
               </div>
               <div class="form-group">
                 <label for="tipe">Tipe</label>
                 <input type="text" class="form-control" id="tipe" name="tipe" placeholder="tipe">
               </div>
               <div class="form-group">
                 <label for="foto">Foto</label>
                 <input type="file" class="form-control" id="foto" name="foto">
               </div>
            </div>
             <div class="modal-footer">
               <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
               <button type="submit" class="btn btn-primary">Tambah Data</button>
             </div>
         </form>
      </div>
    </div>