<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<link rel="stylesheet" type="text/css"  href="<?= BASEURL ?>/css/style-login.css" >
</head>
<body>
	<div class="card">
		<h3>Login</h3>
        <?php 
        if(isset($_GET['status'])){
           echo '<em> '.$_GET['status'].' </em>';
        }
        ?>
		<form action="<?= BASEURL ?>/login/login" method="post" method="POST">
			<label>Username</label>
			<input type="text" name="username" class="form-control" placeholder="username">
 
			<label>Password</label>
			<input type="password" name="password" class="form-control" placeholder="password">

			<input type="checkbox" name="remember-me"> Remember me
       		
 
			<input type="submit" class="tombol" value="LOGIN" name="login">
		</form>
		
	</div>
 
 
</body>
</html>