<?php 

class Users extends Controller {

    public function index()
    {
        if( !isset($_SESSION['login'])) {
            header('Location:' . BASEURL . '/home');
            exit;
        }
        
        $data['users'] = $this->model('usersModel')->getAllUsers();
        $this->view('templates/header', $data);
        $this->view('users/index', $data);
        $this->view('templates/footer', $data);
    }

    public function detail($id) {
        $data['users'] = $this->model('usersModel')->getSingleUsers($id);
        $this->view('templates/header', $data);
        $this->view('users/detail', $data);
        $this->view('templates/footer', $data);
    }

    public function tambah() {

        if($this->model('usersModel')->tambahDataUsers($_POST) > 0){
            Flasher::setFlash('berhasil','ditambahkan','success');
            header('Location:' . BASEURL . '/users');
            exit;
        }
        else {
            Flasher::setFlash('gagal','ditambahkan','danger');
            header('Location:' . BASEURL . '/users');
            exit;
        }
        
    }

    public function getUbah() {
        echo json_encode($this->model('usersModel')->getSingleUsers($_POST['id']));
    }

    public function ubah() {
        if($this->model('usersModel')->ubahDataUsers($_POST) > 0){
            Flasher::setFlash('berhasil','diubah','success');
            header('Location:' . BASEURL . '/users');
            exit;
        }
        else {
            Flasher::setFlash('gagal','diubah','danger');
            header('Location:' . BASEURL . '/users');
            exit;
        }
    }


    public function delete($id) {
        if($this->model('usersModel')->deleteDataUsers($id) > 0){
            Flasher::setFlash('berhasil','dihapus','success');
            header('Location:' . BASEURL . '/users');
            exit;
        }
        else {
            Flasher::setFlash('gagal','dihapus','danger');
            header('Location:' . BASEURL . '/users');
            exit;
        }
    }


}

?>