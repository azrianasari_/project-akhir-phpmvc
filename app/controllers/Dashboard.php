<?php 

class Dashboard extends Controller {

    public function index()
    {
        if( !isset($_SESSION['login'])) {
            header('Location:' . BASEURL . '/home');
            exit;
        } 
        $this->view('templates/header',$_SESSION);
        $this->view('dashboard/index');
        $this->view('templates/footer');
    }


}

?>