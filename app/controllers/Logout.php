<?php 

class Logout extends Controller {
    
    public function index()
    {
        session_start();
        session_destroy();

        setcookie('id','', time()-300);
        setcookie('key','', time()-300);

        header('Location:' . BASEURL . '/home');
        exit;
    }
}

?>