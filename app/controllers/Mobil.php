<?php 

class Mobil extends Controller {

    public function index()
    {
        if( !isset($_SESSION['login'])) {
            header('Location:' . BASEURL . '/home');
            exit;
        }
        
        $data['mobil'] = $this->model('mobilModel')->getAllMobil();
        $data['pemilik'] = $this->model('mobilModel')->getAllPemilik();
        $this->view('templates/header', $data);
        $this->view('mobil/index', $data);
        $this->view('templates/footer', $data);
    }

    public function detail($id) {
        $data['mobil'] = $this->model('mobilModel')->getSingleMobil($id);
        $this->view('templates/header', $data);
        $this->view('mobil/detail', $data);
        $this->view('templates/footer', $data);
    }

    public function tambah() {

        if($this->model('mobilModel')->tambahDataMobil($_POST) > 0){
            Flasher::setFlash('berhasil','ditambahkan','success');
            header('Location:' . BASEURL . '/mobil');
            exit;
        }
        else {
            Flasher::setFlash('gagal','ditambahkan','danger');
            header('Location:' . BASEURL . '/mobil');
            exit;
        }
        
    }

    public function getUbah() {
        echo json_encode($this->model('mobilModel')->getSingleMobil($_POST['id']));
    }

    public function ubah() {
        if($this->model('mobilModel')->ubahDataMobil($_POST) > 0){
            Flasher::setFlash('berhasil','diubah','success');
            header('Location:' . BASEURL . '/mobil');
            exit;
        }
        else {
            Flasher::setFlash('gagal','diubah','danger');
            header('Location:' . BASEURL . '/mobil');
            exit;
        }
    }


    public function delete($id) {
        if($this->model('mobilModel')->deleteDataMobil($id) > 0){
            Flasher::setFlash('berhasil','dihapus','success');
            header('Location:' . BASEURL . '/mobil');
            exit;
        }
        else {
            Flasher::setFlash('gagal','dihapus','danger');
            header('Location:' . BASEURL . '/mobil');
            exit;
        }
    }


}

?>