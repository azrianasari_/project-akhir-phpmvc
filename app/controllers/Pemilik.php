<?php 

class Pemilik extends Controller {

    public function index()
    {
        if( !isset($_SESSION['login'])) {
            header('Location:' . BASEURL . '/home');
            exit;
        }
        
        $data['pemilik'] = $this->model('pemilikModel')->getAllPemilik();
        $this->view('templates/header', $data);
        $this->view('pemilik/index', $data);
        $this->view('templates/footer', $data);
    }

    public function detail($id) {
        $data['pemilik'] = $this->model('pemilikModel')->getSinglePemilik($id);
        $this->view('templates/header', $data);
        $this->view('pemilik/detail', $data);
        $this->view('templates/footer', $data);
    }

    public function tambah() {
        if($this->model('pemilikModel')->tambahDataPemilik($_POST) > 0){
            Flasher::setFlash('berhasil','ditambahkan','success');
            header('Location:' . BASEURL . '/pemilik');
            exit;
        }
        else {
            Flasher::setFlash('gagal','ditambahkan','danger');
            header('Location:' . BASEURL . '/pemilik');
            exit;
        }
        
    }

    public function getUbah() {
        echo json_encode($this->model('pemilikModel')->getSinglePemilik($_POST['id']));
    }

    public function ubah() {
        if($this->model('pemilikModel')->ubahDataPemilik($_POST) > 0){
            Flasher::setFlash('berhasil','diubah','success');
            header('Location:' . BASEURL . '/pemilik');
            exit;
        }
        else {
            Flasher::setFlash('gagal','diubah','danger');
            header('Location:' . BASEURL . '/pemilik');
            exit;
        }
    }


    public function delete($id) {
        if($this->model('pemilikModel')->deleteDataPemilik($id) > 0){
            Flasher::setFlash('berhasil','dihapus','success');
            header('Location:' . BASEURL . '/pemilik');
            exit;
        }
        else {
            Flasher::setFlash('gagal','dihapus','danger');
            header('Location:' . BASEURL . '/pemilik');
            exit;
        }
    }


}

?>