<?php 

class mobilModel {

    private $tabel = 'mobils';
    private $db;

    public function __construct() 
    {
        $this->db = new Database;
    }
  
 
    public function getAllMobil() {
        $this->db->query('select '. $this->tabel .'.id, '. $this->tabel .'.kode, '. $this->tabel .'.status_mobil,pemiliks.nama from '. $this->tabel .' LEFT JOIN pemiliks  ON '. $this->tabel .'.pemilik_id = pemiliks.id');
 
        return $this->db->resultSet();
    }  

    public function getAllPemilik() {
        $this->db->query('SELECT * FROM pemiliks');
        return $this->db->resultSet();
    }  

    public function getSingleMobil($id) {
        $this->db->query('SELECT * FROM '. $this->tabel.' WHERE id = :id');
        $this->db->bind('id',$id);

        return $this->db->single();
    } 
    
    public function tambahDataMobil($data) {
        $nama = $_FILES['foto']['name'];
        $file_tmp = $_FILES['foto']['tmp_name'];	
       
        if(move_uploaded_file($file_tmp, 'img/'.$nama)){
          
        $query = "INSERT INTO mobils 
        (id, kode, tahun, warna, no_plat, no_mesin, no_rangka, status_mobil, merk, tipe, foto, pemilik_id ) VALUES 
        ('', :kode, :tahun, :warna, :no_plat, :no_mesin, :no_rangka, :status_mobil, :merk, :tipe ,:foto, :pemilik_id)";

        $this->db->query($query);
        $this->db->bind('kode', $data['kode']);
        $this->db->bind('tahun', $data['tahun']);
        $this->db->bind('warna', $data['warna']);
        $this->db->bind('no_plat', $data['no_plat']);
        $this->db->bind('no_mesin', $data['no_mesin']);
        $this->db->bind('no_rangka', $data['no_rangka']);
        $this->db->bind('status_mobil', $data['status_mobil']);
        $this->db->bind('merk', $data['merk']);
        $this->db->bind('tipe', $data['tipe']);
        $this->db->bind('foto', $nama);
        $this->db->bind('pemilik_id', $data['pemilik_id']);
        $this->db->execute();

        }
        return $this->db->rowCount();
    }

     
    public function ubahDataMobil($data) {

        if($_FILES['foto']['name'] != "") {
            $nama = $_FILES['foto']['name'];
            $file_tmp = $_FILES['foto']['tmp_name'];	
            move_uploaded_file($file_tmp, 'img/'.$nama);
        } else {
            $nama = $this->getSingleMobil($data['id'])['foto'];
        }
      
        $query = "UPDATE mobils
        SET kode = :kode,tahun = :tahun,warna = :warna,no_plat = :no_plat,no_mesin = :no_mesin,no_rangka = :no_rangka,status_mobil = :status_mobil,merk = :merk,foto = :foto,pemilik_id = :pemilik_id WHERE id = :id;";

        $this->db->query($query);
        $this->db->bind('kode', $data['kode']);
        $this->db->bind('tahun', $data['tahun']);
        $this->db->bind('warna', $data['warna']);
        $this->db->bind('no_plat', $data['no_plat']);
        $this->db->bind('no_mesin', $data['no_mesin']);
        $this->db->bind('no_rangka', $data['no_rangka']);
        $this->db->bind('status_mobil', $data['status_mobil']);
        $this->db->bind('merk', $data['merk']);
        $this->db->bind('foto', $nama);
        $this->db->bind('pemilik_id', $data['pemilik_id']);
        $this->db->bind('id', $data['id']);
      
        $this->db->execute();

        return $this->db->rowCount();
    }

    public function deleteDataMobil($id) {
        $query = "DELETE FROM mobils WHERE id = :id";
        $this->db->query($query);
        $this->db->bind('id',$id);
        $this->db->execute();

        return $this->db->rowCount();
    }

}

?>