<?php 

class usersModel {

    private $tabel = 'users';
    private $db;

    public function __construct() 
    {
        $this->db = new Database;
    }
  
 
    public function getAllUsers() {
        $this->db->query('SELECT * FROM '. $this->tabel);
        return $this->db->resultSet();
    }  

    public function getSingleUsers($id) {
        $this->db->query('SELECT * FROM '. $this->tabel.' WHERE id = :id');
        $this->db->bind('id',$id);

        return $this->db->single();
    } 
    
    public function tambahDataUsers($data) {
        // var_dump($data);die();
        $password = md5($data['password']);

        $query = "INSERT INTO users VALUES ('',:username, :password, :jabatan)";

        $this->db->query($query);
        $this->db->bind('username', $data['username']);
        $this->db->bind('password', $password);
        $this->db->bind('jabatan', $data['jabatan']);
        $this->db->execute();

        return $this->db->rowCount();
    }

     
    public function ubahDataUsers($data) {

        if($data['password'] != "") {
            $password = $data['password'];
        } else {
            $password = $this->getSingleUsers($data['id'])['password'];
        }

        $query = "UPDATE users
        SET username = :username,
            password = :password,
            jabatan = :jabatan
        WHERE id = :id;";

        $this->db->query($query);
        $this->db->bind('username', $data['username']);
        $this->db->bind('password', $password);
        $this->db->bind('jabatan', $data['jabatan']);
        $this->db->bind('id', $data['id']);
     
        $this->db->execute();

        return $this->db->rowCount();
    }

    public function deleteDataUsers($id) {
        $query = "DELETE FROM users WHERE id = :id";
        $this->db->query($query);
        $this->db->bind('id',$id);
        $this->db->execute();

        return $this->db->rowCount();
    }

    public function LoginUsers($data) {
        // menangkap data yang dikirim dari form
         $username  = $_POST["username"];
         $password  = md5($_POST["password"]);

         $result = $this->db->query('SELECT * FROM '. $this->tabel.' WHERE username = :username AND password = :password');
         $this->db->bind('username',$username);
         $this->db->bind('password',$password);
         $this->db->execute();
         $this->db->single();
            // var_dump($this->db->rowCount() == 1);die();
        //  return $this->db->rowCount();
    
        // menghitung jumlah data yang ditemukan
        if ( $this->db->rowCount() == 1 ){
            return $this->db->single();
        }
    }
 


}

?>