<?php 

class pemilikModel {

    private $tabel = 'pemiliks';
    private $db;

    public function __construct() 
    {
        $this->db = new Database;
    }
  
 
    public function getAllPemilik() {
      
        $this->db->query('SELECT * FROM '. $this->tabel);
        return $this->db->resultSet();
    }  

    public function getSinglePemilik($id) {
        $this->db->query('SELECT * FROM '. $this->tabel.' WHERE id = :id');
        $this->db->bind('id',$id);

        return $this->db->single();
    } 
    
    public function tambahDataPemilik($data) {
       
       
        $query = "INSERT INTO pemiliks (id, nama, alamat, kelurahan, kecamatan, kab_kota, kode, kode_pos, telp)
                                VALUES ('',:nama, :alamat, :kelurahan, :kecamatan, :kab_kota, :kode, :kode_pos, :telp)";

        $this->db->query($query);
        $this->db->bind('nama', $data['nama']);
        $this->db->bind('alamat', $data['alamat']);
        $this->db->bind('kelurahan', $data['kelurahan']);
        $this->db->bind('kecamatan', $data['kecamatan']);
        $this->db->bind('kab_kota', $data['kab_kota']);
        $this->db->bind('kode', $data['kode']);
        $this->db->bind('kode_pos', $data['kode_pos']);
        $this->db->bind('telp', $data['telp']);
        $this->db->execute();

        return $this->db->rowCount();
    }

     
    public function ubahDataPemilik($data) {
      
        $query = "UPDATE pemiliks
        SET nama = :nama,
            alamat = :alamat,
            kelurahan = :kelurahan,
            kecamatan = :kecamatan,
            kab_kota = :kab_kota,
            kode = :kode,
            kode_pos = :kode_pos,
            telp = :telp
            WHERE id = :id;";

        $this->db->query($query);
        $this->db->bind('nama', $data['nama']);
        $this->db->bind('alamat', $data['alamat']);
        $this->db->bind('kelurahan', $data['kelurahan']);
        $this->db->bind('kecamatan', $data['kecamatan']);
        $this->db->bind('kab_kota', $data['kab_kota']);
        $this->db->bind('kode', $data['kode']);
        $this->db->bind('kode_pos', $data['kode_pos']);
        $this->db->bind('telp', $data['telp']);
        $this->db->bind('id', $data['id']);
      
        $this->db->execute();

        return $this->db->rowCount();
    }

    public function deleteDataPemilik($id) {
        $query = "DELETE FROM pemiliks WHERE id = :id";
        $this->db->query($query);
        $this->db->bind('id',$id);
        $this->db->execute();

        return $this->db->rowCount();
    }

}

?>